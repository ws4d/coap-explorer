package application;

public class RequestDestination {

	private String host;
	private int port;
	private String path;
	private String query;
	
	public RequestDestination(String address) {
		
		String myAddress = address;
		if (address.startsWith("coap://") | address.startsWith("coaps://")) {
			myAddress = address.split("//")[1];
		}
		this.host = myAddress.split("/",0)[0];
		this.path = myAddress.substring(this.host.length());
		
		this.port = 5683;
		if(this.host.contains(":")) {
			this.port = Integer.parseInt(this.host.split(":")[1]);
			this.host = this.host.split(":")[0];
		}	
	}
	
	public String getHost() {
		return this.host;
	}

	public int getPort() {
		return this.port;
	}

	public String getPath() {
		return this.path;
	}

	public String getQuery() {
		return this.query;
	}
}
