package application;

public class RequestOptions {
	
	public String payload;
	public String proxyURI;
	public String eTag;
	
	public String ifMatch;
	public Boolean isIfMatch;	
	public Boolean ifNoneMatch;
	
	public Boolean observe;
	public Boolean confirmable;
		
	public Integer size1;
	public Integer size2;
	public Integer accept;
	public Integer contentType;
		
	public RequestOptions() {}
}
