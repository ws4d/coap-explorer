package application;

import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import coap.CoapClientInterface;
import coap.californium.CaliforniumCoapClient;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

public class UserInterfaceController implements CoapHandler {

	private CoapClientInterface coapClient = new CaliforniumCoapClient(this);

	@FXML
	private TextArea contentBox;
	@FXML
	private TextField addressBar;
	@FXML
	private TreeView<String> treeView;
	@FXML
	private CheckBox chk_confirmable;
	@FXML
	private CheckBox chk_observe;
	@FXML
	private CheckBox chk_ifMatch;
	@FXML
	private CheckBox chk_ifNoneMatch;
	@FXML
	private TextField tf_eTag;
	@FXML
	private TextField tf_proxy;
	@FXML
	private TextField tf_ifMatch;
	@FXML
	private ChoiceBox<?> choice_blockwise;
	@FXML
	private Spinner<Integer> spn_blocksize1;
	@FXML
	private Spinner<Integer> spn_blocksize2;
	@FXML
	private Spinner<Integer> spn_size1;
	@FXML
	private Spinner<Integer> spn_size2;
	@FXML
	private ComboBox<String> cmb_accept;
	@FXML
	private ChoiceBox<String> cmb_contentType;

	@FXML
	protected void handleDiscoveryAction(ActionEvent event) {
		this.coapClient.MulticastDiscovery();
		this.contentBox.setText("Discovery button pressed");
	}

	@FXML
	protected void handleGetAction(ActionEvent event) {
		RequestDestination dest = new RequestDestination(this.addressBar.getText());
		this.coapClient.getResource(dest.getHost(), dest.getPort(), dest.getPath(), getOptions());
	}

	@FXML
	protected void handlePostAction(ActionEvent event) {
		RequestDestination dest = new RequestDestination(this.addressBar.getText());
		this.coapClient.postResource(dest.getHost(), dest.getPort(), dest.getPath(), getOptions());
	}

	@FXML
	protected void handlePutAction(ActionEvent event) {
		RequestDestination dest = new RequestDestination(this.addressBar.getText());
		this.coapClient.putResource(dest.getHost(), dest.getPort(), dest.getPath(), getOptions());
	}

	@FXML
	protected void handleDeleteAction(ActionEvent event) {
		RequestDestination dest = new RequestDestination(this.addressBar.getText());
		this.coapClient.deleteResource(dest.getHost(), dest.getPort(), dest.getPath(), getOptions());
	}


	@SuppressWarnings("static-method")
	@FXML
	protected void handleCloseAction(ActionEvent event) {
		System.exit(0);
	}

	private RequestOptions getOptions() {
		RequestOptions opt = new RequestOptions();

		opt.payload = this.contentBox.getText();

		opt.confirmable = this.chk_confirmable.isSelected();

		if (this.tf_proxy.getText().trim().length() > 0) {
			opt.proxyURI = this.tf_proxy.getText().trim();
		}
		if (this.tf_eTag.getText().trim().length() > 0) {
			opt.eTag = this.tf_eTag.getText().trim();
		}
		opt.isIfMatch = this.chk_ifMatch.isSelected();
		opt.ifMatch = this.tf_ifMatch.getText().trim();
		opt.ifNoneMatch = this.chk_ifNoneMatch.isSelected();
		opt.observe = this.chk_observe.isSelected();
		opt.accept = MediaTypeRegistry.parse(this.cmb_accept.getValue());
		opt.contentType = MediaTypeRegistry.parse(this.cmb_contentType.getValue());
		opt.contentType = opt.contentType == -1 ? 0 : opt.contentType;
		return opt;
	}

	public void onLoad(CoapResponse response) { // also error resp.
		// System.out.println(response.getResponseText());
		if (null != response.getPayload()) {
			this.contentBox.setText(response.getResponseText());
		} else {
			this.contentBox.setText(response.getCode().name());
		}
	}

	public synchronized void handleCoREFormat(CoapResponse response) {

		// Error response? -> do not try to find resources
		if (response.getCode().codeClass != 2) {
			onLoad(response);
			return;
		}

		// Root existing?
		TreeItem<String> root = this.treeView.getRoot();
		if (root == null) {
			root = new TreeItem<>();
			this.treeView.setRoot(root);
		}
		
		this.treeView.getSelectionModel().clearSelection();
		
		// Host already contained? -> if not add it
		String host = response.advanced().getSourceContext().getPeerAddress().getHostName();
		if (response.advanced().getSourceContext().getPeerAddress().getPort() != 5683) {
			host += ":" + response.advanced().getSourceContext().getPeerAddress().getPort();
		}

		ObservableList<TreeItem<String>> hosts = root.getChildren();
		TreeItem<String> hostItem = new TreeItem<>(host);
		boolean found = false;
		for (TreeItem<String> item : hosts) {
			if (item.getValue().equals(host)) {
				hostItem = item;
				found = true;
				break;
			}
		}
		if (!found) {
			hosts.add(hostItem);
		}

		// add resources
		String[] resources = response.getResponseText().split(",");
		hostItem.getChildren().clear();
		for (String resource : resources) {
			String path = resource.trim().split(">")[0].substring(1);
			hostItem.getChildren().add(new TreeItem<>(path));
		}

		hostItem.setExpanded(true);

		// Display response
		onLoad(response);
	}

	public void onError() { // I/O errors and timeouts
		System.err.println("Failed: I/O error or timeout");
	}

	public void initialize() {
		ObservableList<String> list = this.cmb_accept.getItems();

		for (Integer type : MediaTypeRegistry.getAllMediaTypes()) {
			list.add(new String(MediaTypeRegistry.toString(type)));
		}
		this.cmb_accept.setItems(list);
		this.cmb_accept.getSelectionModel().select(MediaTypeRegistry.toString(MediaTypeRegistry.UNDEFINED));
		this.cmb_contentType.setItems(list);
		this.cmb_contentType.getSelectionModel().select(MediaTypeRegistry.toString(MediaTypeRegistry.UNDEFINED));
		this.treeView.setShowRoot(false);

		TextField address = this.addressBar;
		this.treeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<String>>() {

			public void changed(ObservableValue<? extends TreeItem<String>> observable, TreeItem<String> oldValue,
					TreeItem<String> newValue) {

				if (newValue != null) {
					String value = "";
					if (newValue.getParent().getValue() != null) {
						value = newValue.getParent().getValue();
					}
					value += newValue.getValue();
					address.setText(value);
				}
			}

		});
	}
}