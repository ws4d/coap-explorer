package coap;

import application.RequestOptions;

public interface CoapClientInterface {
	
	void MulticastDiscovery();
	
	void queryResourceDirectory(String serverAddress, int serverPort);

	void getResource(String serverAddress, int i, String path, RequestOptions reqOpts);
	void putResource(String serverAddress, int i, String path, RequestOptions reqOpts);
	void postResource(String serverAddress, int i, String path, RequestOptions reqOpts);
	void deleteResource(String serverAddress, int i, String path, RequestOptions reqOpts);
}
