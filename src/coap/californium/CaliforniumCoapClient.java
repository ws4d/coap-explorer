/* MIT License
 * 
 * Copyright (c) 2017 University of Rostock - Institute of Applied Microelectronics and Computer Engineering
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package coap.californium;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.OptionSet;
import org.eclipse.californium.core.coap.Request;

import application.RequestOptions;
import application.UserInterfaceController;
import coap.CoapClientInterface;

public class CaliforniumCoapClient implements CoapClientInterface{
	
	private UserInterfaceController handler;
	private CoapObserveRelation obsRel;
	
	public CaliforniumCoapClient(UserInterfaceController handler) {
		this.handler=handler;
	}
	
	public void MulticastDiscovery() {
		
		cancelObserve();
		
		List<String> addresses = new ArrayList<>();
		addresses.add("224.0.1.187");//IPV4_MC_ADDR
		addresses.add("ff02::fd");//IPV6_LL_MC_ADDR
		addresses.add("ff05::fd");//IPV6_SL_MC_ADDR
		
		for (String address : addresses) {
			CoapClient client = new CoapClient(address+"/.well-known/core");
			
			UserInterfaceController localHandler = this.handler;
			client.get(new CoapHandler(){

				public void onLoad(CoapResponse response) {
					localHandler.handleCoREFormat(response);				
				}

				public void onError() {
					localHandler.onError();
				}
				
			});
		}
	}
	
	public void queryResourceDirectory(String serverAddress, int serverPort) {
		
		cancelObserve();
		
		CoapClient client = new CoapClient(serverAddress+":"+serverPort+"/rd-lookup/res");
		Request request = Request.newGet();
		request.setConfirmable(true);
		client.advanced(this.handler, request);
	}
	
	public void getResource(String serverAddress, int serverPort, String path, RequestOptions options) {
		
		cancelObserve();
		
		CoapClient client = new CoapClient(serverAddress+":"+serverPort+path);
		Request request = Request.newGet();
		processStandardRequestOptions(request, options);
		
		//Process specific options
//		OptionSet opt = request.getOptions();
//		if(options.observe) {
//			opt.setObserve(0);
//		}
		if(path.contentEquals("/.well-known/core")) {
			UserInterfaceController localHandler = this.handler;
			client.get(new CoapHandler(){

				public void onLoad(CoapResponse response) {
					localHandler.handleCoREFormat(response);				
				}

				public void onError() {
					localHandler.onError();
				}
				
			});
		}
		if(options.observe) {
			this.obsRel = client.observe(this.handler);
			return;
		}
		client.advanced(this.handler, request);
	}
	
	public void putResource(String serverAddress, int serverPort, String path, RequestOptions options) {
		
		cancelObserve();
		
		CoapClient client = new CoapClient(serverAddress+":"+serverPort+path);
		Request request = Request.newPut();
		processStandardRequestOptions(request, options);
		
		//Process specific options
		request.setPayload(options.payload);
		OptionSet opt = request.getOptions();
		opt.setContentFormat(options.contentType);
		
		client.advanced(this.handler, request);
	}
	
	public void postResource(String serverAddress, int serverPort, String path, RequestOptions options) {
		
		cancelObserve();
		
		CoapClient client = new CoapClient(serverAddress+":"+serverPort+path);
		Request request = Request.newPost();
		processStandardRequestOptions(request, options);
		
		//Process specific options
		request.setPayload(options.payload);
		OptionSet opt = request.getOptions();
		opt.setContentFormat(options.contentType);
		
		client.advanced(this.handler, request);
	}
	
	public void deleteResource(String serverAddress, int serverPort, String path, RequestOptions options) {
		
		cancelObserve();
		
		CoapClient client = new CoapClient(serverAddress+":"+serverPort+path);
		Request request = Request.newDelete();
		processStandardRequestOptions(request, options);
		
		client.advanced(this.handler, request);
	}
	
	private void cancelObserve() {
		if(this.obsRel != null) {
			this.obsRel.proactiveCancel();
			this.obsRel = null;
		}
	}
	
	private static void processStandardRequestOptions(Request request, RequestOptions reqOps){
		
		request.setConfirmable(reqOps.confirmable);
		
		OptionSet opt = request.getOptions();
		if(reqOps.proxyURI != null) {
			opt.setProxyUri(reqOps.proxyURI);
		}
		if(reqOps.eTag != null) {
			opt.addETag(reqOps.eTag.getBytes());
		}
		if(reqOps.accept != -1) {
			opt.setAccept(reqOps.accept);
		}
		if(reqOps.isIfMatch) {
			opt.addIfMatch(reqOps.ifMatch.getBytes());
		}
		opt.setIfNoneMatch(reqOps.ifNoneMatch);
	}
}
